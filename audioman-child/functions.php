<?php
require_once __DIR__ . '/vendor/autoload.php';
include('shortcodes/price-tables.php');
include('shortcodes/get-audios.php');
include('shortcodes/soundcloud-embed.php');
include('shortcodes/featured-service.php');
include('shortcodes/contact-details.php');
include('shortcodes/map-embed.php');
include('shortcodes/ap-heading.php');

// Change dashboard Posts to Audios
add_action( 'init', 'ap_change_posttype_audio' );
function ap_change_posttype_audio() {
    $get_post_type = get_post_type_object('post');
    $labels = $get_post_type->labels;
        $labels->name = 'Audios';
        $labels->singular_name = 'Audio';
        $labels->add_new = 'Add Audio File';
        $labels->add_new_item = 'Add Audio File';
        $labels->edit_item = 'Edit Audio File';
        $labels->new_item = 'News Audio';
        $labels->view_item = 'View Audio File';
        $labels->search_items = 'Search Audio Files';
        $labels->not_found = 'No News Posts found';
        $labels->not_found_in_trash = 'No News Posts found in Trash';
        $labels->all_items = 'All Audio Posts';
        $labels->menu_name = 'Audios';
        $labels->name_admin_bar = 'Audios';
}

register_nav_menus( array(
	'privacy-footer'       => esc_html__( 'Footer Privacy Menu', 'audioman' ),
) );

//Remove Product Galleries
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

add_action( 'wp_enqueue_scripts', 'ap_enqueue_scripts' );
function ap_enqueue_scripts() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );	
	wp_enqueue_style( 'child-style', get_stylesheet_directory_uri() . '/style.css', array( 'parent-style' ), wp_get_theme()->get('Version'));
	wp_enqueue_script( 'ap-scripts', get_stylesheet_directory_uri() . '/assets/js/app.js', array( 'jquery' ));
	wp_enqueue_script( 'load-fa', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.8.2/js/all.min.js' );
}

/*Remove Admin Bar For All Users except admin*/
add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  show_admin_bar(false);
	}
}