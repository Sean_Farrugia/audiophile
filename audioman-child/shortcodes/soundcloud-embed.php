<?php
function ap_get_soundcloud_embed( $atts ) { ?>
	<div class="soundcloud-link">
		<iframe width="100%" height="166" scrolling="no" frameborder="no" allow="autoplay" src="<?php echo $atts["embedlink"] ?>"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"></div>
	</div>
<?php 
}
add_shortcode( 'ap_soundcloud_link', 'ap_get_soundcloud_embed' );