<?php
function ap_get_pricing_tables( $atts ) { ?>
<div class="pricing-cards">
  <div class="card-body">

	<h5 class="card-title text-muted text-uppercase text-center">Mixing</h5>
	<h6 class="card-price text-center">€ <?php echo $atts["priceone"]; ?><span class="period"> / Track</span></h6>
	<hr>
	<ul class="fa-ul">
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Mixing</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>12 Track Limit</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>WAV File Format</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Revisions</li>
	  <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Ready for Publishing</li>
	  <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Mastered Stem Downloads</li>
	  <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Unlimited Tracks</li>
	  <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>AIFF and FLACC Export</li>
	</ul>
	<a href="<?php echo $atts["linkone"]; ?>" class="btn btn-block btn-primary text-uppercase">View Service</a>
  </div>
  <!-- Plus Tier -->
  <div class="card-body">
	<h5 class="card-title text-muted text-uppercase text-center">Mastering</h5>
	<h6 class="card-price text-center">€ <?php echo $atts["pricetwo"]; ?><span class="period"> / Track</span></h6>
	<hr>
	<ul class="fa-ul">
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Mastering</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>1 Mixed Audio Track</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>WAV File Format</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Revisions</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Ready for Publishing</li>
	  <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Mastered Stem Downloads</li>
	  <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>Mixing of Stems</li>
	  <li class="text-muted"><span class="fa-li"><i class="fas fa-times"></i></span>AIFF and FLACC Export</li>
	</ul>
	<a href="<?php echo $atts["linktwo"]; ?>" class="btn btn-block btn-primary text-uppercase">View Service</a>
  </div>
  <!-- Pro Tier -->
  <div class="card-body">
	<h5 class="card-title text-muted text-uppercase text-center">Mixing/Mastering</h5>
	<h6 class="card-price text-center">€ <?php echo $atts["pricethree"] ?><span class="period"> / Track</span></h6>
	<hr>
	<ul class="fa-ul">
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Mixing + Mastering</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>1 Audio Upload</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>WAV File Format</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Unlimited Revisions</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Ready for Publishing</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Mastered Stem Downloads</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>Mixing of Stems</li>
	  <li><span class="fa-li"><i class="fas fa-check"></i></span>AIFF and FLACC Export</li>
	</ul>
	<a href="<?php echo $atts["linkthree"]; ?>" class="btn btn-block btn-primary text-uppercase">View Service</a>
  </div>
</div>
<?php 
}
add_shortcode( 'ap_get_services', 'ap_get_pricing_tables' );