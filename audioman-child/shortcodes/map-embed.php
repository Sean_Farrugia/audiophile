<?php
function ap_get_map_embed( $atts ) { ?>
	<div class="map-link">
		<iframe src="<?php echo $atts["embedlink"] ?>" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
	</div>
<?php 
}
add_shortcode( 'ap_map_embed', 'ap_get_map_embed' );