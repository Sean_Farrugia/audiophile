<?php
function ap_heading_func( $atts ) { ?>
	<div class="wrap">
	  <h2 class="centre-line"><span><?php echo $atts["title"] ?></span></h2>
	</div>
<?php 
}
add_shortcode( 'ap_heading', 'ap_heading_func' );