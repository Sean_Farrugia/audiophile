<?php
function ap_get_contact_details( $atts ) { ?>
	<div class="contact-details-section">
		<div class="wpb_wrapper">
			<p class="contact-title">
				<strong>
					<span><?php echo $atts["company"]; ?></span>
				</strong>
			</p>
			<p>
				<i class="fa fa-compass"></i><?php echo $atts["address"]; ?>
			</p>
			<p>
				<i class="fa fa-phone"></i> 
				<a class="footer-contact-m" href="tel:00356<?php echo $atts["phone"]; ?>"> +356 <?php echo $atts["phone"]; ?></a>
			</p>
			<p><i class="fa fa-envelope"></i> <a href="mailto:<?php echo $atts["email"]; ?>"><?php echo $atts["email"]; ?></a></p>
		</div>
	</div>
<?php 
}
add_shortcode( 'ap_contact_details', 'ap_get_contact_details' );