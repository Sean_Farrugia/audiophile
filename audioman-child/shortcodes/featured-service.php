<?php
function ap_featured_post_func( $atts ) { 
	$args = array(
		'post_type' => 'product',
		'posts_per_page' => 1,
		'id' => $atts["id"],
		);
	$loop = new WP_Query( $args );
	if ( $loop->have_posts() ) {
		while ( $loop->have_posts() ) : $loop->the_post();
			$_product = wc_get_product( get_the_ID() );
			$thumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' );
	?>
			<a href="<?php echo the_permalink() ?>">
				<div class="featured-service" style="background-image: url(<?php echo $thumbnail[0] ?>)">
					<h2 class="featured-title"><?php echo the_title(); ?></h2>
					<h5><?php echo "€".$_product->get_regular_price(); ?></h5>
				</div>
			</a>
	<?php
		endwhile;
	} else {
		echo __( 'No products found' );
	}
	wp_reset_postdata();
?>
	
<?php 
}
add_shortcode( 'ap_featured_service', 'ap_featured_post_func' );