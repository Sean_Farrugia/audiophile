<?php
/**
 * Display Header Media
 *
 * @package Audioman
 */
?>

<?php
	global $post;
	$header_image = audioman_featured_overall_image();

	if ( 'disable' === $header_image ) {
		// Bail if all header media are disabled.
		return;
	}
	/*ADDED TO CHANGE HEADER IMAGE IN WOOCOMMERCE PAGES*/
	if(is_product()){
		$page_header_img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
	}
?>
<div class="custom-header header-media">
	<div class="wrapper">
		<?php if ( ( is_header_video_active() && has_header_video() ) || 'disable' !== $header_image ) : ?>
		<div class="custom-header-media"  <?php if(is_product()){ echo "style='background-image:url(".$page_header_img[0].")'"; } ?> >
			<?php
			if ( is_header_video_active() && has_header_video() ) {
				the_custom_header_markup();
			} elseif ( $header_image ) {
				echo '<div id="wp-custom-header" class="wp-custom-header"><img src="' . esc_url( $header_image ) . '"/></div>	';
			}
			?>

			<?php audioman_header_media_text(); ?>
		</div>
		<?php endif; ?>
	</div><!-- .wrapper -->
	<div class="custom-header-overlay"></div><!-- .custom-header-overlay -->
</div><!-- .custom-header -->