<?php
/**
 * Displays footer widgets if assigned
 *
 * @package Audioman
 */

?>

<?php
if (
	 is_active_sidebar( 'sidebar-2' ) ||
	 is_active_sidebar( 'sidebar-3' ) ||
	 is_active_sidebar( 'sidebar-4' ) ) :
?>

<aside <?php audioman_footer_sidebar_class(); ?> role="complementary">
	<div class="wrapper">
			<div class="widget-column footer-widget-1">
				<?php if ( has_nav_menu( 'privacy-footer' ) ) { ?>
				<nav id="privacy-footer-navigation" class="privacy-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Footer Privacy Links Menu', 'audioman' ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'privacy-footer',
							'menu_class'     => 'privacy-links-menu',
						) );
					?>
				</nav>
			<?php }else{
				dynamic_sidebar( 'sidebar-2' );
			} ?>
				<?php  ?>
			</div><!-- .widget-area -->

		<?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
			<div class="widget-column footer-widget-2">
				<?php dynamic_sidebar( 'sidebar-3' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>

		<?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
			<div class="widget-column footer-widget-3">
				<?php dynamic_sidebar( 'sidebar-4' ); ?>
			</div><!-- .widget-area -->
		<?php endif; ?>
	</div><!-- .footer-widgets-wrapper -->
</aside><!-- .footer-widgets -->

<?php endif;
